/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package makingSudoku;

import java.io.FileNotFoundException;

/**
 *
 * @author MajnikSzA
 */
public class SudokuTest {

    public static void main(String[] args) {

        int[][] table = new int[][]{
            {0, 0, 0, 0, 0, 0, 2, 0, 0},
            {0, 5, 4, 1, 0, 6, 0, 0, 9},
            {0, 7, 0, 4, 0, 0, 0, 1, 8},
            {0, 4, 6, 2, 0, 0, 3, 0, 0},
            {7, 0, 0, 0, 0, 0, 1, 4, 0},
            {0, 8, 0, 0, 0, 0, 0, 0, 6},
            {8, 0, 0, 0, 1, 4, 0, 0, 0},
            {0, 0, 1, 0, 5, 0, 0, 0, 0},
            {5, 6, 7, 0, 9, 0, 0, 0, 0}};

        Sudoku s = new Sudoku(table);

        s.diagonal = false;
    }

    public boolean solveTheRiddle(Sudoku s) {
        int result = solveIt(s.table, s.diagonal, 1);
        if (result > 0) {
            guess(s.table, s.diagonal);
        }
        if (result == -1) {
            System.out.println("A kezdőadatok ellentmondanak");
            System.out.println("Nem megoldható");
        }
        System.out.println("\n a főtábla");
        printTheTable(s.table);
        return (result != -1);
    }

    public static void printTheTable(int table[][]) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                System.out.print(table[i][j] + " ");
                if ((j + 1) % 3 == 0) {
                    System.out.print(" ");
                }
            }
            System.out.println("");
            if ((i + 1) % 3 == 0) {
                System.out.println(" ");
            }
        }
    }

    /**
     * Ez a mainből kiszervezés azért kell, hogy megoldható legyen, hogy a nem
     * triviális megoldások miatti elakadás itt tippeléssel is mehessen tovább,
     * anélkül, hogy szétbarmolnánk az eredeti, és csak biztos adatokat
     * tartalmazó table [][] mátrixot.
     *
     */
    public static int solveIt(int table[][], boolean d, int write) {
        Sudoku p = new Sudoku(table);
        p.diagonal = d;
        int[][] copyTable = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                copyTable[i][j] = table[i][j];
            }
        }

        p.table = copyTable;
        int lastResult;
        int counter = 0;
        do {

            counter++;
            lastResult = p.countMissing(p.table);

            if (p.callThe4SolvingMethod(p.table)) {
                return -1;
            }
        } while (p.countMissing(p.table) < lastResult);

        int empty = p.countMissing(p.table);

        if ((write == 1) || (write == 0 && empty == 0)) {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    table[i][j] = copyTable[i][j];
                }
            }
            printTheTable(p.table);

        }

        return empty;

    }

    /**
     * Ha az első solveIt algoritmus hagyott üres mezőket, de ellentmondás nincs
     * akkor ez a függvény tippel sorban minden lehetőségre, hátha jön ki jó
     * eredmény. Ha egy tipp ellentmondást eredményez, akkor azt a pontot
     * false-ra állítjuk a possible [][][] mátrixban.
     *
     * @param table
     * @param d
     */
    public static void guess(int table[][], boolean d) {

        Sudoku g = new Sudoku(table);
        g.diagonal = d;
        int[][] copyTable = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                copyTable[i][j] = table[i][j];
            }
        }
        g.table = copyTable;

        g.callThe4SolvingMethod(table);

        int success = 0;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                for (int k = 9; k > 0; k--) {
                    if (g.possible[i][j][k] && g.table[i][j] == 0) {

                        g.table[i][j] = k;

                        success = solveIt(g.table, d, 0);

                        if (success == -1) {
                            g.possible[i][j][k] = false;
                            g.table[i][j] = 0;
                        }
                        if (success == 0) {

                            printTheTable(g.table);
                            for (int n = 0; n < 9; n++) {
                                for (int m = 0; m < 9; m++) {
                                    table[n][m] = copyTable[n][m];
                                }
                            }
                        }
                        if (success > 0) {
                            g.table[i][j] = 0;
                        }
                    }
                }
            }
        }
    }
}
