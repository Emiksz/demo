/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package makingSudoku;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author MajnikSzA
 */
public class ViewOfSudoku extends JFrame implements ActionListener {

    JTextField[][] cells;
    JTextField message;
    JButton solve, hint, continueToRidle, save;
    JCheckBox d;
    Sudoku sudokuOnScreen;

    public ViewOfSudoku() {
        // showTheMatrix(table);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(null);
        this.setLocation(200, 100);
        this.setSize(320, 450);
        this.setResizable(false);

        cells = new JTextField[9][9];

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                cells[i][j] = new JTextField(1);
                cells[i][j].setBounds(10 + j * 30 + 10 * (int) (j / 3),
                        10 + i * 30 + 10 * (int) (i / 3), 25, 25);
                add(cells[i][j]);
            }
        }
        message = new JTextField("1-9 közti egészekkel tölthető minden fenti cella");
        message.setBounds(10, 300, 285, 25);
        add(message);
        continueToRidle = new JButton("rejtvény");
        continueToRidle.setBounds(10, 335, 85, 25);
        add(continueToRidle);
        continueToRidle.addActionListener(this);
        solve = new JButton("megold");
        solve.setBounds(110, 335, 85, 25);
        add(solve);
        solve.addActionListener(this);

        hint = new JButton("Tipp");
        hint.setBounds(80, 370, 70, 25);
        add(hint);
        hint.addActionListener(this);

        save = new JButton("mentés");
        save.setBounds(210, 335, 85, 25);

        add(save);
        save.addActionListener(this);

        d = new JCheckBox("Átlós");
        d.setBounds(10, 370, 60, 25);
        add(d);
        d.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getSource() == d) {
                    if (e.getStateChange() == 1) {
                        sudokuOnScreen.diagonal = true;
                    } else {
                        sudokuOnScreen.diagonal = false;
                    }
                    paintDiagonal(sudokuOnScreen.diagonal);
                }
            }
        });

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String buttonName = e.getActionCommand();
        if (buttonName.equals("rejtvény")) {
            //       System.out.println("gomb megnyomva");
            makerButtonPressed();
        }
        if (buttonName.equals("Tipp")) {
            hintButtonPressed();
        }
        if (buttonName.equals("megold")) {
            solveButtonPressed();
        }
        if (buttonName.equals("mentés")) {
            readWindowNumbers();
            try {
                makingSudoku.CsvWriter.toStringLists(sudokuOnScreen.table, message.getText());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ViewOfSudoku.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void makerButtonPressed() {
        if (readWindowNumbers()) {
            MakeARiddle mr = new MakeARiddle();
            mr.makeARandomRiddle(sudokuOnScreen);
            if (mr.getRoundOfRandom() == 200) {
                message.setText("Sikertelen randomszámok. Nyomd meg újra!");
            }
        }
    }

    public boolean readWindowNumbers() {
        int n = 0;
        boolean legalInput = true;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (!cells[i][j].getText().equals("")) {
                    n = Integer.parseInt(cells[i][j].getText());
                    if (n < 0 || n > 9) {
                        legalInput = false;
                        setMessage("csak 1-9 közti egészek lehetnek!");
                    }
                }
            }
        }
        if (legalInput) {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    if (!cells[i][j].getText().equals("")) {
                        sudokuOnScreen.table[i][j] = Integer.parseInt(cells[i][j].getText());
                    } else {
                        sudokuOnScreen.table[i][j] = 0;
                    }
                }
            }
        }
        return legalInput;
    }

    public void hintButtonPressed() {
        if (readWindowNumbers()) {
            paintWhite();
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    for (int k = 1; k < 10; k++) {
                        sudokuOnScreen.possible[i][j][k] = true;
                    }
                }
            }

            sudokuOnScreen.initPossible(sudokuOnScreen.table);
            sudokuOnScreen.surelyNot(sudokuOnScreen.table); //ez átlót is kezel
            int countFalse = 0;
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    countFalse = 0;
                    for (int f = 1; f < 10; f++) {
                        if (sudokuOnScreen.possible[i][j][f] == false) {
                            countFalse++;
                        }
                    }

                    if (countFalse == 7) {
                        cells[i][j].setBackground(Color.yellow);
                    }
                    if (countFalse == 9) {
                        cells[i][j].setBackground(Color.red);
                    }
                }
            }

            int[][] copyTable = new int[9][9];
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    copyTable[i][j] = sudokuOnScreen.table[i][j];
                }
            }
            Sudoku hint = new Sudoku(copyTable);
            hint.diagonal = sudokuOnScreen.diagonal;
            hint.callThe4SolvingMethod(copyTable);
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    if (hint.table[i][j] > sudokuOnScreen.table[i][j]) {
                        cells[i][j].setBackground(Color.green);
                    }
                }
            }
        }
    }

    public void solveButtonPressed() {
        readWindowNumbers();
        SudokuTest st = new SudokuTest();
        boolean ready = st.solveTheRiddle(sudokuOnScreen);
        String message;
        if (ready) {
            if (sudokuOnScreen.countMissing(sudokuOnScreen.table) == 0) {
                message = "Megoldva!";
            } else {
                message = "Több adat kell a megoldáshoz!";
            }
        } else {
            message = "Ellentmondó kezdőadatok!";
        }
        drawMyTable(sudokuOnScreen, message);
    }

    public void fillTheView(ViewOfSudoku v) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (v.sudokuOnScreen.table[i][j] > 0) {
                    v.cells[i][j].setText("" + v.sudokuOnScreen.table[i][j]);
                }
            }
        }
    }

    public void paintWhite() {

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                cells[i][j].setBackground(Color.white);
            }
        }
        paintDiagonal(sudokuOnScreen.diagonal);
    }

    public void paintDiagonal(boolean diagonal) {
        Color c;
        if (diagonal) {
            c = Color.cyan;
        } else {
            c = Color.white;
        }
        for (int i = 0; i < 9; i++) {
            cells[i][i].setBackground(c);
            cells[i][8 - i].setBackground(c);
        }
    }

    public Sudoku getSudokuOnScreen() {
        return sudokuOnScreen;
    }

    public void setSudokuOnScreen(Sudoku sudokuOnScreen) {
        this.sudokuOnScreen = sudokuOnScreen;
    }

    public static void main(String[] args) {
        int[][] table = new int[][]{
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0}};

        ViewOfSudoku v = new ViewOfSudoku();
        v.sudokuOnScreen = new Sudoku(table);
        v.fillTheView(v);
    }

    public static void drawMyTable(Sudoku s, String m) {
        ViewOfSudoku v = new ViewOfSudoku();
        v.sudokuOnScreen = new Sudoku(s.table);
        v.fillTheView(v);
        v.message.setText(m);
        v.d.setSelected(s.diagonal);
        v.paintDiagonal(s.diagonal);
    }

    public void setMessage(String message) {
        this.message.setText(message);
    }

}
