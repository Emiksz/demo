/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package makingSudoku;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MajnikSzA
 */
public class MakeARiddle {

    private int roundOfRandom = 0;

    public static void main(String[] args) {
        MakeARiddle mr = new MakeARiddle();
        int[][] table = new int[][]{
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0}};
       
        Sudoku r = new Sudoku(table);
        r.diagonal = false;
        mr.makeARandomRiddle(r);
    }

    public void makeARandomRiddle(Sudoku r)  {
        int success;
        do {
            insertRandomNumber(r);
            roundOfRandom++;
            success = SudokuTest.solveIt(r.table, r.diagonal, -1);

        } while (success > 0&&roundOfRandom<200);
        if (roundOfRandom==200){
            
        }
        if (success == 0) {

            SudokuTest.printTheTable(r.table);
            ViewOfSudoku.drawMyTable(r, "Ez egy kész rejtvény");
           
        }
        if (success == -1) {
            ViewOfSudoku.drawMyTable(r, "Ellentmondó kezdőadatok!");
        }
        success = SudokuTest.solveIt(r.table, r.diagonal, 1);

    }

    public static int giveRandom9() {
        return (int) (Math.random() * 9 + 1);
    }

    public static void insertRandomNumber(Sudoku r)  {

        int i, j, k, result;
        do {
            i = giveRandom9() - 1;
            j = giveRandom9() - 1;
            k = giveRandom9();
        } while (r.possible[i][j][k] == false);
        if (r.table[i][j] == 0) {
            r.table[i][j] = k;
            result = SudokuTest.solveIt(r.table, r.diagonal, -1);
            if (result == -1) {
                r.table[i][j] = 0;
            }
        }
    }

    public int getRoundOfRandom() {
        return roundOfRandom;
    }
}
