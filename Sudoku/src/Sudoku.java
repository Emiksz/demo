/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package makingSudoku;

/**
 *
 * @author Majnik Szabolcs
 */
public class Sudoku {

    /**
     * a table tömb lesz az, ahol vezetjük az ismert számokat, ezek lehetnek
     * kezdőadatok és menet közben kiszámolt eredmények is. A még ismeretlen
     * cellák értéke 0 kell legyen.
     */
    public int table[][] = new int[9][9];

    /**
     * a possible tömb a következőt jelenti: possible [0][4][7]= true esetén az
     * első sor 5. eleme még lehet a hetes. possible [0][4][7]= false esetén a
     * hetes lehetősége abban a cellában már kizárt. possible [x][y][0] elemeket
     * nem használjuk, de [][][9] kell.
     */
    public boolean possible[][][];

    boolean diagonal;

    // private int position[] = new int[2]; //későbbi fejlesztéshez.

    public Sudoku(int table[][]) {
        this.table = table;
        possible = new boolean[9][9][10];

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                for (int k = 1; k < 10; k++) {
                    possible[i][j][k] = true;
                }
            }
        }
    }

    /**
     * A table [][] még megfejtetlen celláit számolja meg
     *
     * @param table
     * @return
     */
    public int countMissing(int table[][]) {
        int empty = 0;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (table[i][j] == 0) {
                    empty++;
                }
            }
        }
        return empty;
    }

    public boolean callThe4SolvingMethod(int table[][]) {
        initPossible(table);
        surelyNot(table);
        return (thereCanBeOnlyOne(table) || thereIsNoOtherPlace(table));
        // true = ellentmondó adatok, false = nincs hiba
    }

    /**
     * A possible tömbbe beírjuk a table [][] már ismert adatait, tehát a biztos
     * számok helyén csak a beírt számot hagyjuk meg true-nak a possible [i][j]
     * 9 eleméből, a többit nyolcat false-ra állítjuk.
     *
     * @param table
     */
    public void initPossible(int table[][]) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (table[i][j] != 0) {
                    for (int k = 1; k < 10; k++) {
                        if (table[i][j] == k) {
                            possible[i][j][k] = true;
                        } else {
                            possible[i][j][k] = false;
                        }
                    }
                }
            }
        }
    }

    public void surelyNot(int table[][]) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (table[i][j] != 0) {  
                    for (int c = 0; c < 9; c++) {
                        if (j != c) {
                            possible[i][c][table[i][j]] = false; 
                        }
                    }
                    for (int r = 0; r < 9; r++) {
                        if (i != r) {
                            possible[r][j][table[i][j]] = false; 
                        }
                    }
                    int f = 3 * ((int) (i / 3)); 
                    int g = 3 * ((int) (j / 3));  
                    for (int k = f; k < f + 3; k++) { 
                        for (int l = g; l < g + 3; l++) {
                            if (k != i || l != j) {
                                possible[k][l][table[i][j]] = false;
                            }
                        }
                    }
                    if (diagonal) {
                        if (i == j) {
                            for (int d = 0; d < 9; d++) {
                                if (d != i) {
                                    possible[d][d][table[i][j]] = false;
                                }
                            }
                        }
                        if (i + j == 8) {
                            for (int d = 0; d < 9; d++) {
                                if (8 - d != i) {
                                    possible[8 - d][d][table[i][j]] = false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /** Ha van olyan [i][j] mező a Possible mátrixában, amelyhez már
     * csak egyetlen true és 8 false tartozik, akkor az a mező eldőlt,
     * a true helye beírható a table mátrixába. Ha valahol 9 false 
     * jött össze, akkor az adatok ellentmondóak, nincs megoldás. Ekkor
     * true-t ad vissza, egyébként false-t. 
     * @param table 
     */
    public boolean thereCanBeOnlyOne(int table[][]) {

        int countFalse, foundTrueHere;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                countFalse = 0;
                foundTrueHere = 0;
                for (int f = 1; f < 10; f++) {
                    if (possible[i][j][f] == false) {
                        countFalse++;
                    } else {
                        foundTrueHere = f;
                    }
                }
                if (countFalse == 8) {
                    table[i][j] = foundTrueHere;
                }
                if (countFalse == 9) {
                    System.out.println("hiba van az adatokban");
                    return true;  
                }
            }
        }
        return false; 
    }

    /**
     soronként, oszloponként és mezőnként végigjárjuk a possible
     [][] táblát, és olyan területekre vadászunk, ahol valamelyik konkrét
     szám csak egyetlen példányban szerepel. 
     Pl: ha a possible[][][] 3. sorában csak és kizárólag az 5. oszlopban 
     true a kilences elem, akkor a tabla[2][4]=9 lesz
     Ha egy ilyen területen valamelyik szám egyáltalán nem található meg,
     az ellentmondást jelent, tehát return true kell
     */
    public boolean thereIsNoOtherPlace(int table[][]) {
        int[] counter = new int[9];

        for (int i = 0; i < 9; i++) { 
            for (int n = 0; n < 9; n++) {
                counter[n] = 0;
            }
            for (int j = 0; j < 9; j++) {
                for (int k = 1; k < 10; k++) {
                    if (possible[i][j][k] == true) {
                        counter[k - 1]++;
                    }
                }
            }
            for (int k = 0; k < 9; k++) {
                if (counter[k] == 0) {
                    return true;
                }
                if (counter[k] == 1) { 
                    for (int j = 0; j < 9; j++) {
                        if (possible[i][j][k + 1] == true) {
                            table[i][j] = k + 1;
                        }
                    }
                }
            }
        }

        for (int j = 0; j < 9; j++) { 
            for (int n = 0; n < 9; n++) {
                counter[n] = 0;
            }
            for (int i = 0; i < 9; i++) {
                for (int k = 1; k < 10; k++) {
                    if (possible[i][j][k] == true) {
                        counter[k - 1]++;
                    }
                }
            }
            for (int k = 0; k < 9; k++) {
                if (counter[k] == 0) {
                    return true;
                }
                if (counter[k] == 1) { 
                    for (int i = 0; i < 9; i++) {
                        if (possible[i][j][k + 1] == true) {
                            table[i][j] = k + 1;
                        }
                    }
                }
            }
        }

        for (int i = 0; i < 3; i++) {   
            for (int j = 0; j < 3; j++) {
                for (int n = 0; n < 9; n++) {
                    counter[n] = 0;
                }
                for (int r = 0; r < 3; r++) {
                    for (int c = 0; c < 3; c++) {
                        for (int k = 1; k < 10; k++) {
                            if (possible[i * 3 + r][j * 3 + c][k] == true) {
                                counter[k - 1]++;
                            }
                        }
                    }
                }
                for (int k = 0; k < 9; k++) {
                    if (counter[k] == 0) {
                        return true;
                    }
                    if (counter[k] == 1) { 
                        for (int r = 0; r < 3; r++) {
                            for (int c = 0; c < 3; c++) {
                                if (possible[3 * i + r][3 * j + c][k + 1] == true) {
                                    table[3 * i + r][3 * j + c] = k + 1;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (diagonal) {
            for (int n = 0; n < 9; n++) {  
                counter[n] = 0;
            }
            for (int i = 0; i < 9; i++) {
                for (int k = 1; k < 10; k++) {
                    if (possible[i][i][k] == true) {
                        counter[k - 1]++;
                    }
                }
            }
            for (int k = 0; k < 9; k++) {
                if (counter[k] == 0) {
                    return true;
                }
                if (counter[k] == 1) { 
                    for (int i = 0; i < 9; i++) {
                        if (possible[i][i][k + 1] == true) {
                            table[i][i] = k + 1;
                        }
                    }
                }
            }

            for (int n = 0; n < 9; n++) { 
                counter[n] = 0;
            }
            for (int i = 0; i < 9; i++) {
                for (int k = 1; k < 10; k++) {
                    if (possible[i][8 - i][k] == true) {
                        counter[k - 1]++;
                    }
                }
            }
            for (int k = 0; k < 9; k++) {
                if (counter[k] == 0) {
                    return true;
                }
                if (counter[k] == 1) { 
                    for (int i = 0; i < 9; i++) {
                        if (possible[i][8 - i][k + 1] == true) {
                            table[i][8 - i] = k + 1;
                        }
                    }
                }
            }
        }
        return false;
    }   
}
