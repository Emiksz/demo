/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package makingSudoku;

/**
 *
 * @author MajnikSzA
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class CsvWriter {

    public static void toStringLists(int[][] ridle, String name) throws FileNotFoundException {

        List<List<String>> stringToWrite = new ArrayList<>();
        //   List<String> row = new ArrayList<>();

        for (int i = 0; i < 9; i++) {
            stringToWrite.add(new ArrayList<String>());
            for (int j = 0; j < 9; j++) {
                stringToWrite.get(i).add("" + ridle[i][j]);
            }
        }
        stringToWrite.add(new ArrayList<String>());

        CsvWriter csvWriter = new CsvWriter();
        System.out.println(stringToWrite.toString());
        writeToCsv(stringToWrite, name);
    }

    public static void writeToCsv(List<List<String>> text, String name) throws FileNotFoundException {

        PrintWriter pw;
        StringBuilder sb;

        pw = new PrintWriter(new File("sudoku" + name + ".csv"));
        sb = new StringBuilder();
        String c = "";
        for (int i = 0; i < text.size(); i++) {
            for (int j = 0; j < text.get(i).size(); j++) {
                c = text.get(i).get(j);
                if ("0".equals(c)) {
                    c = "";
                }
                sb.append(c);
                if (j != text.get(i).size() - 1) {
                    sb.append(";");  //Excelhez, egyébként ','
                }
            }
            sb.append("\n");
        }
        pw.write(sb.toString());
        pw.close();

    }
}
